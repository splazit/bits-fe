(function() {
  'use strict';

  angular
    .module('bits', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'ngMaterial']);

})();
