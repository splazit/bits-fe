(function() {
  'use strict';

  angular
    .module('bits')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
