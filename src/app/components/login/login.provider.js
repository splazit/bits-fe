(function() {
  'use strict';

  angular
    .module('bits')
    .provider('LoginProvider', LoginProvider);

  /** @ngInject */
  function LoginProvider() {
    var vm = this;
    vm.loginUrl = '';
    vm.token = '';

    this.setLoginUrl = function(loginUrl){
      vm.loginUrl = loginUrl;
    };

    this.$get = [function () {

    }];
  }

})();
