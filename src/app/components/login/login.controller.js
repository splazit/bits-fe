(function() {
  'use strict';

  angular
    .module('bits')
    .controller('LoginController', LoginController);

  function LoginController() {
    this.username = '';
    this.password = '';
    this.email = '';
    this.address = '';
  }

})();
